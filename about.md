---
layout: page
title: About
permalink: /about/
---

This site hosts and documents high quality open source projects.
The site itself is a FOSS project under the [MIT license](https://opensource.org/licenses/MIT).

We test our site with [BrowserStack](https://www.browserstack.com/open-source)!

<!-- ### More Information

This site is still undergoing development.
-->

### Contact me
If site related, feel free to an open issue in this website's source [repository](https://github.com/pro-src/website).
